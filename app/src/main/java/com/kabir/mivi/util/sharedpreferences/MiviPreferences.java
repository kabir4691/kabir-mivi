package com.kabir.mivi.util.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.kabir.mivi.MiviApplication;

public class MiviPreferences implements Constants {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private String FILE_NAME = "mivi_shared_preferences";

    public MiviPreferences() {
        sharedPreferences = MiviApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    @NonNull
    private SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    @NonNull
    private SharedPreferences.Editor getEditor() {
        if (editor == null) {
            editor = getSharedPreferences().edit();
        }
        return editor;
    }

    public boolean getIsUserLoggedIn() {
        return getSharedPreferences().getBoolean(KEY_IS_USER_LOGGED_IN, false);
    }

    public MiviPreferences setIsUserLoggedIn(boolean value) {
        getEditor().putBoolean(KEY_IS_USER_LOGGED_IN, value)
                   .commit();
        return this;
    }
}
