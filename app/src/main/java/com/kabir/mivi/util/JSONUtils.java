package com.kabir.mivi.util;

import android.content.res.Resources;

import com.kabir.mivi.R;
import com.kabir.mivi.MiviApplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public class JSONUtils {

    public static JSONObject getCollection() throws IOException, JSONException {
        Resources resources = MiviApplication.getInstance().getResources();
        InputStream is = resources.openRawResource(R.raw.collection);
        Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        int n;
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        while ((n = reader.read(buffer)) != -1) {
            writer.write(buffer, 0, n);
        }
        is.close();
        String text = writer.toString();
        return new JSONObject(text);
    }
}
