package com.kabir.mivi.util.sharedpreferences;

interface Constants {

    String KEY_IS_USER_LOGGED_IN = "is_user_logged_in";
    String KEY_CONTEST_1_ID = "contest_1_id";
}
