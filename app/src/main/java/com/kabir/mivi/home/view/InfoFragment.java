package com.kabir.mivi.home.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.Size;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kabir.mivi.R;
import com.kabir.mivi.home.model.AttributeItem;

import java.util.ArrayList;

public class InfoFragment extends Fragment {

    public static final String KEY_LIST = "list";

    private ArrayList<AttributeItem> attributeList;

    private LayoutInflater layoutInflater;
    private RecyclerView contentRecyclerView;

    public static InfoFragment newInstance(@NonNull @Size(min = 1) ArrayList<AttributeItem> list) {
        Bundle args = new Bundle();
        args.putParcelableArrayList(KEY_LIST, list);
        InfoFragment fragment = new InfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        attributeList = getArguments().getParcelableArrayList(KEY_LIST);
        layoutInflater = LayoutInflater.from(getContext());
    }

    @Nullable @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_info, container, false);

        contentRecyclerView = view.findViewById(R.id.content_recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        contentRecyclerView.setLayoutManager(layoutManager);
        contentRecyclerView.setHasFixedSize(true);

        AttributeAdapter adapter = new AttributeAdapter(layoutInflater, attributeList);
        contentRecyclerView.setAdapter(adapter);

        return view;
    }
}
