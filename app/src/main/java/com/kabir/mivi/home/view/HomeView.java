package com.kabir.mivi.home.view;

import android.support.annotation.NonNull;
import android.support.annotation.Size;

import com.kabir.mivi.home.model.InfoItem;

import java.util.ArrayList;

public interface HomeView {

    void setContent(@NonNull @Size(min = 1) ArrayList<InfoItem> list);
}
