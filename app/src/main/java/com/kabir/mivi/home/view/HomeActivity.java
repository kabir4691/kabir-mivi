package com.kabir.mivi.home.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.kabir.mivi.R;
import com.kabir.mivi.home.model.InfoItem;
import com.kabir.mivi.home.presenter.HomePresenter;
import com.kabir.mivi.home.presenter.HomePresenterImpl;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity implements HomeView {

    private TabLayout typeTabLayout;
    private ViewPager contentViewPager;
    private InfoPagerAdapter infoPagerAdapter;

    private HomePresenter homePresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);

        typeTabLayout = findViewById(R.id.type_tab_layout);
        contentViewPager = findViewById(R.id.content_view_pager);

        typeTabLayout.setupWithViewPager(contentViewPager);

        homePresenter = new HomePresenterImpl(this);
        homePresenter.start();
    }

    @Override
    public void setContent(@NonNull ArrayList<InfoItem> list) {
        infoPagerAdapter = new InfoPagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < list.size(); i++) {
            InfoItem infoItem = list.get(i);
            String title = infoItem.getTitle();
            InfoFragment fragment = InfoFragment.newInstance(infoItem.getAttributeItemList());
            infoPagerAdapter.addFragment(title, fragment);
        }
        contentViewPager.setAdapter(infoPagerAdapter);
    }
}
