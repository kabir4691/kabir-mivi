package com.kabir.mivi.home.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class AttributeItem implements Parcelable{

    private String name;
    private String value;

    public AttributeItem(@NonNull String name, @NonNull String value) {
        this.name = name;
        this.value = value;
    }

    protected AttributeItem(Parcel in) {
        name = in.readString();
        value = in.readString();
    }

    public static final Creator<AttributeItem> CREATOR = new Creator<AttributeItem>() {
        @Override
        public AttributeItem createFromParcel(Parcel in) {
            return new AttributeItem(in);
        }

        @Override
        public AttributeItem[] newArray(int size) {
            return new AttributeItem[size];
        }
    };

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getValue() {
        return value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(value);
    }
}
