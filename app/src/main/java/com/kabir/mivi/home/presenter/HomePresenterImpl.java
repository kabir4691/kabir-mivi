package com.kabir.mivi.home.presenter;

import android.support.annotation.NonNull;

import com.kabir.mivi.home.model.AttributeItem;
import com.kabir.mivi.home.model.InfoItem;
import com.kabir.mivi.home.view.HomeView;
import com.kabir.mivi.util.JSONUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class HomePresenterImpl implements HomePresenter {

    private HomeView homeView;

    public HomePresenterImpl(@NonNull HomeView homeView) {
        this.homeView = homeView;
    }

    @Override
    public void start() {
        ArrayList<InfoItem> infoItemList = new ArrayList<>();
        try {
            JSONObject collectionObject = JSONUtils.getCollection();
            JSONArray includedArray = collectionObject.getJSONArray("included");
            for (int i = 0; i < includedArray.length(); i++) {
                JSONObject includedObject = includedArray.getJSONObject(i);

                String type = includedObject.getString("type");

                JSONObject attributesObject = includedObject.getJSONObject("attributes");
                Iterator<String> keys = attributesObject.keys();
                ArrayList<AttributeItem> attributeList = new ArrayList<>();
                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = String.valueOf(attributesObject.get(key));
                    AttributeItem attributeItem = new AttributeItem(key, value);
                    attributeList.add(attributeItem);
                }

                InfoItem infoItem = new InfoItem(type, attributeList);
                infoItemList.add(infoItem);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        homeView.setContent(infoItemList);
    }
}
