package com.kabir.mivi.home.view;

import android.support.annotation.NonNull;
import android.support.annotation.Size;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kabir.mivi.R;
import com.kabir.mivi.home.model.AttributeItem;

import java.util.List;

class AttributeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater layoutInflater;
    private List<AttributeItem> attributeItemList;

    AttributeAdapter(@NonNull LayoutInflater inflater,
                     @NonNull @Size(min = 1) List<AttributeItem> list) {
        layoutInflater = inflater;
        attributeItemList = list;
    }

    @NonNull @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AttributeViewHolder(layoutInflater.inflate(R.layout.layout_home_attribute, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AttributeViewHolder viewHolder = (AttributeViewHolder) holder;
        AttributeItem attributeItem = attributeItemList.get(position);
        viewHolder.nameTextView.setText(attributeItem.getName());
        viewHolder.valueTextView.setText(attributeItem.getValue());
    }

    @Override
    public int getItemCount() {
        return attributeItemList.size();
    }

    private class AttributeViewHolder extends RecyclerView.ViewHolder {

        private TextView nameTextView;
        private TextView valueTextView;

        private AttributeViewHolder(View itemView) {
            super(itemView);

            nameTextView = itemView.findViewById(R.id.name_text_view);
            valueTextView = itemView.findViewById(R.id.value_text_view);
        }
    }
}
