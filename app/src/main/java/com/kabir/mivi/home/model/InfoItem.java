package com.kabir.mivi.home.model;

import android.support.annotation.NonNull;
import android.support.annotation.Size;

import java.util.ArrayList;

public class InfoItem {

    private String title;
    private ArrayList<AttributeItem> attributeItemList;

    public InfoItem(@NonNull String title, @NonNull @Size(min = 1) ArrayList<AttributeItem> list) {
        this.title = title;
        this.attributeItemList = list;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    @NonNull
    public ArrayList<AttributeItem> getAttributeItemList() {
        return attributeItemList;
    }
}
