package com.kabir.mivi;

import android.app.Application;

public class MiviApplication extends Application {

    private static MiviApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static synchronized MiviApplication getInstance() {
        return instance;
    }
}
