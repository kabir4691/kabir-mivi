package com.kabir.mivi.login.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kabir.mivi.R;
import com.kabir.mivi.login.presenter.LoginPresenter;
import com.kabir.mivi.login.presenter.LoginPresenterImpl;
import com.kabir.mivi.welcome.WelcomeActivity;

public class LoginActivity extends AppCompatActivity implements LoginView {

    private EditText userIdEditText;
    private EditText passwordEditText;
    private TextView submitTextView;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        userIdEditText = findViewById(R.id.user_id_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        submitTextView = findViewById(R.id.submit_text_view);

        submitTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginPresenter.attemptLogin(userIdEditText.getText().toString(),
                                            passwordEditText.getText().toString());
            }
        });

        loginPresenter = new LoginPresenterImpl(this);
        loginPresenter.start();
    }

    @Override
    public void showInvalidUserIdError() {
        userIdEditText.setError("Invalid user ID");
    }

    @Override
    public void showInvalidPasswordError() {
        passwordEditText.setError("Invalid password");
    }

    @Override
    public void showLoginFailedError() {
        Toast.makeText(this, "Invalid login credentials", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToWelcome() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        supportFinishAfterTransition();
    }
}
