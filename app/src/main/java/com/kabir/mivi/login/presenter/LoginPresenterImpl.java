package com.kabir.mivi.login.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.kabir.mivi.login.view.LoginView;
import com.kabir.mivi.util.JSONUtils;
import com.kabir.mivi.util.sharedpreferences.MiviPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView loginView;

    private MiviPreferences miviPreferences;

    public LoginPresenterImpl(@NonNull LoginView loginView) {
        this.loginView = loginView;
    }

    @Override
    public void start() {
        boolean isUserLoggedIn = getMiviPreferences().getIsUserLoggedIn();
        if (isUserLoggedIn) {
            loginView.navigateToWelcome();
        }
    }

    @Override
    public void attemptLogin(String userId, String password) {
        if (TextUtils.isEmpty(userId)) {
            loginView.showInvalidUserIdError();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            loginView.showInvalidPasswordError();
            return;
        }

        String correctUserId, correctPassword;
        try {
            JSONObject jsonObject = JSONUtils.getCollection();
            JSONObject dataObject = jsonObject.getJSONObject("data");
            correctUserId = dataObject.getString("id");
            JSONObject attributesObject = dataObject.getJSONObject("attributes");
            correctPassword = attributesObject.getString("contact-number");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
            loginView.showLoginFailedError();
            return;
        }

        if (!userId.equals(correctUserId)) {
            loginView.showLoginFailedError();
            return;
        }

        if (!password.equals(correctPassword)) {
            loginView.showLoginFailedError();
            return;
        }

        getMiviPreferences().setIsUserLoggedIn(true);

        loginView.navigateToWelcome();
    }

    @NonNull
    private MiviPreferences getMiviPreferences() {
        if (miviPreferences == null) {
            miviPreferences = new MiviPreferences();
        }
        return miviPreferences;
    }
}
