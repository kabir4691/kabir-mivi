package com.kabir.mivi.login.presenter;

public interface LoginPresenter {

    void start();

    void attemptLogin(String userId, String password);
}
