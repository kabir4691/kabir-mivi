package com.kabir.mivi.login.view;

public interface LoginView {

    void showInvalidUserIdError();
    void showInvalidPasswordError();

    void showLoginFailedError();

    void navigateToWelcome();
}
