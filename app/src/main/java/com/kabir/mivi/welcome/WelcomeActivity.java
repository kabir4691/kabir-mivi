package com.kabir.mivi.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.kabir.mivi.R;
import com.kabir.mivi.home.view.HomeActivity;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);

        navigateToHomeAfterDelay();
    }

    private void navigateToHomeAfterDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                navigateToHome();
            }
        }, 500);
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        supportFinishAfterTransition();
        overridePendingTransition(R.transition.activity_slide_enter, R.transition.activity_slide_exit);
    }

    @Override
    public void onBackPressed() {
        // Prevent user from closing this activity due to back press.
    }
}
